package company.teams;

import company.employees.Employee;
import company.employees.FullStackDeveloper;
import company.employees.JavaDeveloper;
import company.employees.TeamLeader;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class TestTeam {

    final JavaDeveloper javaDeveloper1 = new JavaDeveloper("Alex", "4", 1200,"developer");
    final JavaDeveloper javaDeveloper2 = new JavaDeveloper("Oana", "0", 700, "Internship");
    final TeamLeader teamLeader = new TeamLeader();
    final FullStackDeveloper fullStackDeveloper1 = new FullStackDeveloper("Robert", "1", 1000, "asdasd");

    @Test
    public void testBackEndTeam(){

        List<Employee> testList = new ArrayList<Employee>();

        testList.add(javaDeveloper1);
        testList.add(javaDeveloper2);
        testList.add(teamLeader);

        new BackEndTeam(testList);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testBackEndTeam2(){

        List<Employee>testList = new ArrayList<Employee>();

        testList.add(javaDeveloper1);
        testList.add(teamLeader);

        new BackEndTeam(testList);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testBackEndTeam3(){

        List<Employee>testList = new ArrayList<Employee>();

        testList.add(javaDeveloper1);
        testList.add(javaDeveloper2);

        new BackEndTeam(testList);
    }

    @Test
    public void testBackEndTeam4(){

        List<Employee>testList = new ArrayList<Employee>();

        testList.add(fullStackDeveloper1);
        testList.add(javaDeveloper1);
        testList.add(teamLeader);

        new BackEndTeam(testList);
    }

}
