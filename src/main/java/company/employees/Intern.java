package company.employees;

public abstract class Intern {

    private final String name;
    private final InternType internType;

    public Intern(String name, InternType internType) {

        this.name = name;
        this.internType = internType;
    }

    public abstract Mentor.TestAnswer answerToTest(String testRequirements);

    public enum InternType {
        JAVAINTERN("Java Intern"),
        TEAMLEADINTERN("Team Lead Intern");

        private final String whatever;

        InternType(String whatever) {

            this.whatever = whatever;
        }

        public static InternType toInternType(String whatever) {
            InternType[] allType = InternType.values();
            for (InternType type : allType) {
                if (type.getWhatever().equals(whatever)) {
                    return type;
                }
            }
            return null;
        }

        public String getWhatever() {

            return whatever;
        }
    }

    @Override
    public String toString() {
        return "Intern{" +
                "name='" + name + '\'' +
                ", internType=" + internType +
                '}';
    }
}
