package company.employees;

public class WebDeveloper extends Developer {

    public WebDeveloper(String name, String experience, Integer salary, String jobDescription) {
        super(name, experience, salary, jobDescription);
    }
}
