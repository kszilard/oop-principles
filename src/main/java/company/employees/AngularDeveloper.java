package company.employees;

public class AngularDeveloper extends Developer {

    public AngularDeveloper(String name, String experience, Integer salary, String jobDescription) {
        super(name, experience, salary, jobDescription);
    }
}
