package company.employees;

public class FullStackDeveloper extends Developer implements Mentor {

    public FullStackDeveloper(String name, String experience, Integer salary, String jobDescription) {
        super(name, experience, salary, jobDescription);
    }

    public FullStackDeveloper(String experience, Integer salary) {
        super(experience, salary);
    }

    public class JavaIntern extends Intern{

        public JavaIntern(String name, InternType internType){

            super(name, internType);
        }

        public TestAnswer answerToTest(String testRequirements) {
            if(testRequirements.length() % 2 == 0){
                return TestAnswer.COMPLEXANSWER;
            }
            else {
                return TestAnswer.SIMPLEANSWER;
            }
        }
    }
}
