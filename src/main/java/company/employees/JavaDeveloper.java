package company.employees;

public class JavaDeveloper extends Developer {

    public JavaDeveloper(String name, String experience, Integer salary, String jobDescription) {
        super(name, experience, salary, jobDescription);
    }
}
