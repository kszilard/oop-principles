package company.employees;

public class TeamLeader implements Employee, Mentor{

    public class TeamLeadIntern extends Intern{

        public TeamLeadIntern(String name, InternType internType){

            super(name, internType);
        }

        public TestAnswer answerToTest(String testRequirements){
            if(testRequirements.length() % 2 ==0){
                return TestAnswer.SIMPLEANSWER;
            }
            else {
                return TestAnswer.COMPLEXANSWER;
            }
        }
    }
}
