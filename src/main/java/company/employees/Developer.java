package company.employees;

public abstract class Developer implements Employee {

    private String name;
    private String experience;
    private Integer salary;
    private String jobDescription;
    static final String companyName = "";


//    public Developer(){
//        System.out.println("Default constructor");
//    }

    public Developer(String name, String experience, Integer salary, String jobDescription) {
        this.name = name;
        this.experience = experience;
        this.salary = salary;
        this.jobDescription = jobDescription;
    }

    public Developer(String experience, Integer salary) {
        this.experience = experience;
        this.salary = salary;
    }

    public String getName() {
        return name;
    }

    public String getExperience() {
        return experience;
    }

    public Integer getSalary() {
        return salary;
    }

    public String getJobDescription() {
        return jobDescription;
    }

    public void setExperience(String experience) {
        this.experience = experience;
    }

    public void setSalary(Integer salary) {
        this.salary = salary;
    }

    public void setJobDescription(String jobDescription) {
        this.jobDescription = jobDescription;
    }

    @Override
    public String toString() {
        return "Developer{" +
                "name='" + name + '\'' +
                ", experience='" + experience + '\'' +
                ", salary=" + salary +
                ", jobDescription='" + jobDescription + '\'' +
                '}';
    }
}
