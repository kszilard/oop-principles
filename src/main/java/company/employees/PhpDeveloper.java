package company.employees;

public class PhpDeveloper extends Developer {

    public PhpDeveloper(String name, String experience, Integer salary, String jobDescription) {
        super(name, experience, salary, jobDescription);
    }
}
