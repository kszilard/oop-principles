package company.employees;

public interface Mentor {

    enum TestAnswer{
        SIMPLEANSWER,
        COMPLEXANSWER
    }

    class Test{
        private final String testRequirements;
        private final TestAnswer testAnswer;

        public Test(String testRequirements, TestAnswer testAnswer){
            this.testRequirements = testRequirements;
            this.testAnswer = testAnswer;
        }

        public boolean applyTest(Intern intern){
//            boolean answer = true;
//            if(intern.answerToTest(testRequirements) == testAnswer){
//                return answer;
//            }
//            return false;
            return intern.answerToTest(testRequirements) == testAnswer;
        }
    }
}

