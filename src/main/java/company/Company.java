package company;

import com.sun.scenario.effect.impl.sw.sse.SSEBlend_SRC_OUTPeer;
import company.employees.*;
import company.teams.Team;
import company.utils.Utils;
import org.jcp.xml.dsig.internal.SignerOutputStream;

import java.io.File;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

public class Company {

    public static void main(String[] args){
       Developer developer1 = new JavaDeveloper("Alex", "4" , 1400, "engineer");
       Developer developer2 = new AngularDeveloper("Ion","5" , 1000, "arhitect");
       Developer developer3 = new WebDeveloper("Ana","10", 2000, "Regional manager");
       Developer developer4 = new JavaDeveloper("Marian", "2", 1000, "programmer");


       Developer[] developers = new Developer[]{developer1, developer2, developer3, developer4};


        System.out.println("Number of java developers: " + Utils.countNumberOfJavaDevelopers(developers));
        System.out.println("Number of angular developers: " + Utils.countNumberOfDevelopers(developers, Utils.DeveloperTypes.ANGULARDEVELOPER));
        System.out.println("Number of web developers: " + Utils.countNumberOfDevelopersClass(developers, WebDeveloper.class));

        FullStackDeveloper fullStackDeveloper = new FullStackDeveloper("Mihai","5", 1500,"QA");
        FullStackDeveloper.JavaIntern javaIntern1 = fullStackDeveloper.new JavaIntern("Alex", Intern.InternType.JAVAINTERN);

        Mentor.Test test1 = new Mentor.Test("2+2", Mentor.TestAnswer.SIMPLEANSWER);
        Mentor.Test test2 = new Mentor.Test("2*2", Mentor.TestAnswer.SIMPLEANSWER);
        Mentor.Test test3 = new Mentor.Test("2*2/4+2*9-12", Mentor.TestAnswer.COMPLEXANSWER);
        TeamLeader teamLeader = new TeamLeader();

        File file = new File("D:\\Curs Java\\Proiecte JAVA\\oop-principles\\src\\test\\resources\\Intern.txt");
        Utils.processInterns(file, fullStackDeveloper, teamLeader);

    }

}
