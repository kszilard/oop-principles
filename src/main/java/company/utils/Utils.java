package company.utils;


import com.sun.javafx.collections.MappingChange;
import company.employees.*;
import company.teams.Team;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;


public class Utils {

    //Scrieti o metoda statica care sa determine numarul de java developer dintr-un array.
    //numarul java developarilor intr-o echipa.


    public static int countNumberOfJavaDevelopers(Developer[] developers) {
        int counter = 0;
        for (Developer developer : developers) {
            if (developer instanceof JavaDeveloper) {
                counter++;
            }
        }
        return counter;
    }

    public static <developerType> int countNumberOfDevelopersClass(Developer[] developers, Class<developerType> clazz) {
        int counter = 0;
        for (Developer developer : developers) {
            if (clazz.isInstance(developer)) {
                counter++;
            }
        }
        return counter;
    }

    public static Integer countNumberOfDevelopers(Developer[] developers, DeveloperTypes types) {
        int counter = 0;
        for (Developer developer : developers) {
            switch (types) {
                case ANGULARDEVELOPER:
                    if (developer instanceof AngularDeveloper) {
                        counter++;
                    }
                    break;
                case FULLSTACKDEVELOPER:
                    if (developer instanceof FullStackDeveloper) {
                        counter++;
                    }
                    break;
                case JAVADEVELOPER:
                    if (developer instanceof JavaDeveloper) {
                        counter++;
                    }
                    break;
                case PHPDEVELOPER:
                    if (developer instanceof PhpDeveloper) {
                        counter++;
                    }
                    break;
                case WEBDEVELOPER:
                    if (developer instanceof WebDeveloper) {
                        counter++;
                    }
                    break;
            }
        }
        return counter;
    }

    public static Map<Intern.InternType, List<Intern>> processInterns(File file, FullStackDeveloper fullStackDeveloper, TeamLeader teamLeader) {

        if (file == null) {
            System.out.println("Empty file");
            return Collections.emptyMap();
        }

        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(file))) {
            String fileLine;

            Map<Intern.InternType, List<Intern>> map = new HashMap<>();
            List<Intern> javaInternList = new ArrayList<>();
            List<Intern> teamLeadInterList = new ArrayList<>();

            while ((fileLine = bufferedReader.readLine()) != null) {
                //System.out.println(fileLine);
                String[] elements = fileLine.split(";");
                Intern.InternType internType = Intern.InternType.toInternType(elements[0]);

                try {
                    switch (internType) {

                        case JAVAINTERN:
                            Intern javaIntern = fullStackDeveloper.new JavaIntern(elements[1], Intern.InternType.JAVAINTERN);
                            javaInternList.add(javaIntern);
                            map.put(internType, javaInternList);
                            break;

                        case TEAMLEADINTERN:
                            Intern teamLeadIntern = teamLeader.new TeamLeadIntern(elements[1], Intern.InternType.TEAMLEADINTERN);
                            teamLeadInterList.add(teamLeadIntern);
                            map.put(internType, teamLeadInterList);
                            break;
                    }
                }  catch (NullPointerException e2){
                    System.out.println("Wrong text format");
                }
            }
            System.out.println(map);
            return map;

        } catch (IOException e) {
            System.out.println("Exception caught and handled!");
        }
        return Collections.emptyMap();
    }

    public enum DeveloperTypes {
        ANGULARDEVELOPER(),
        FULLSTACKDEVELOPER(),
        JAVADEVELOPER(),
        PHPDEVELOPER(),
        WEBDEVELOPER();

//        private String text;
//
//        DeveloperTypes(String text) {
//            this.text = text;
//        }
//
//        public static DeveloperTypes developerType(String text) {
//            for (DeveloperTypes b : DeveloperTypes.values()) {
//                if (text.equalsIgnoreCase(b.getText())) {
//                    return b;
//                }
//            }
//            throw new IllegalArgumentException("");
    }
}
