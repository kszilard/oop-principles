package company.teams;

import company.employees.AngularDeveloper;
import company.employees.Employee;
import company.employees.FullStackDeveloper;
import company.employees.WebDeveloper;

import javax.swing.plaf.FontUIResource;
import java.util.List;

public class FrontEndTeam extends Team {
    public FrontEndTeam(List<Employee> employees){

        super(employees);
        validateTeam(employees);
    }

    @Override
    public void validateTeam(List<Employee> employees) {
        int counterDeveloper = 0;

        for(Employee employee : employees){
            if(employee instanceof WebDeveloper || employee instanceof AngularDeveloper || employee
            instanceof FullStackDeveloper){
                counterDeveloper++;
            }
        }
        if(counterDeveloper < 2){
            throw  new IllegalArgumentException("Incorrect team composition!");
        }
    }
}
