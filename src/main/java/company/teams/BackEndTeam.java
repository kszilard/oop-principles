package company.teams;

import com.sun.xml.internal.ws.api.model.wsdl.editable.EditableWSDLMessage;
import company.employees.Employee;
import company.employees.FullStackDeveloper;
import company.employees.JavaDeveloper;
import company.employees.TeamLeader;

import java.util.List;

public class BackEndTeam extends Team {

    public BackEndTeam(List<Employee> employees){
        super(employees);
        validateTeam(employees);
    }

    @Override
    public void validateTeam(List<Employee> employees) {

        int countJavaDev = 0;
        int countTeamLeader = 0;

        for(Employee employee : employees){
            if(employee instanceof TeamLeader){
                countTeamLeader++;
            }
            if(employee instanceof JavaDeveloper || employee instanceof FullStackDeveloper){
                countJavaDev++;
            }
        }
        if(countJavaDev < 2 || countTeamLeader != 1){
            throw new IllegalArgumentException("Incorrect team composition!");
        }
    }
}
