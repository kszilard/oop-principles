package company.teams;

import company.employees.Employee;

import java.util.ArrayList;
import java.util.List;

public abstract class Team {

    private final List<Employee> employees;

    public Team(List<Employee> employees) {
        this.employees = employees;
    }

    public abstract void validateTeam(List<Employee> employees);


}
